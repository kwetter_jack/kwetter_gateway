#
# Build stage
#
FROM maven:3.8.5-openjdk-17 AS build
COPY src /home/app/src
COPY pom.xml /home/app
RUN mvn -f /home/app/pom.xml clean install -DskipTests

FROM openjdk:11
COPY --from=build /home/app/target/gateway-0.0.1-SNAPSHOT.jar gateway.jar
EXPOSE 4000
CMD ["java","-jar","gateway.jar"]

